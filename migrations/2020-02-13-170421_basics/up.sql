CREATE TABLE recipes (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  version VARCHAR NOT NULL,
  category VARCHAR NOT NULL,
  revision INT NOT NULL,
  updated_at timestamp NOT NULL,
  updated_by VARCHAR NOT NULL,
  location VARCHAR NOT NULL,
  branch VARCHAR NOT NULL DEFAULT 'master'
);

CREATE TABLE workers (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  location VARCHAR NOT NULL,
  architecture VARCHAR NOT NULL,
  version VARCHAR,
  last_heartbeat timestamp,
  token VARCHAR
);

CREATE TABLE jobs (
  id SERIAL PRIMARY KEY,
  recipe_id INT NOT NULL,
  worker_id INT NOT NULL,
  architecture VARCHAR NOT NULL,
  state VARCHAR NOT NULL DEFAULT 'new',
  FOREIGN KEY(recipe_id) REFERENCES recipes(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
);

