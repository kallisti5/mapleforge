ENGINE ?= docker
VERSION ?= 0.2.0
default:
	cargo clean
	$(ENGINE) build -t quay.io/kallisti5/mapleforge:$(VERSION) .
devdb:
	$(ENGINE) run --rm -p 5432:5432 -d -e POSTGRES_PASSWORD=manthisissecure --name mapledevdb docker.io/postgres:alpine || true
	echo 'DATABASE_URL="postgresql://postgres:manthisissecure@127.0.0.1:5432"' > .env
	echo 'PORTS_REPO="https://github.com/haikuports/haikuports.git"' >> .env
	echo 'CACHE_DIR="/tmp/mapleforge"' >> .env
	sleep 5
	diesel setup
clean:
	$(ENGINE) kill mapledevdb
test:
	$(ENGINE) network create maplebe || true
	$(ENGINE) run --rm -d -e POSTGRES_PASSWORD=manthisissecure --name mapledb --network maplebe docker.io/postgres:alpine || true
	$(ENGINE) run --rm -it --network maplebe -P -e DATABASE_URL=postgresql://postgres:manthisissecure@mapledb:5432 quay.io/kallisti5/mapleforge:$(VERSION)
push:
	$(ENGINE) push quay.io/kallisti5/mapleforge:$(VERSION)
