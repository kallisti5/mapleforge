use std::fmt;
use std::str;

/// A CPU Architecture
#[derive(Debug,PartialEq)]
pub enum Architecture {
    X86gcc2,
    X86_64,
    X86,
    Arm,
    Arm64,
    Riscv32,
    Riscv64,
    Powerpc,
    M68k,
    Sparc64,
}

impl fmt::Display for Architecture {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::X86gcc2 => write!(f, "x86_gcc2"),
            Self::X86_64 => write!(f, "x86_64"),
            Self::X86 => write!(f, "x86"),
            Self::Arm => write!(f, "arm"),
            Self::Arm64 => write!(f, "arm64"),
            Self::Riscv32 => write!(f, "riscv32"),
            Self::Riscv64 => write!(f, "riscv64"),
            Self::Powerpc => write!(f, "ppc"),
            Self::M68k => write!(f, "m68k"),
            Self::Sparc64 => write!(f, "sparc"),
        }
    }
}

impl str::FromStr for Architecture {
    type Err = ();
    fn from_str(s: &str) -> Result<Architecture, ()> {
        match s {
            "x86_gcc2" => Ok(Architecture::X86gcc2),
            "x86_64" => Ok(Architecture::X86_64),
            "x86" => Ok(Architecture::X86),
            "arm" => Ok(Architecture::Arm),
            "arm64" => Ok(Architecture::Arm64),
            "riscv32" => Ok(Architecture::Riscv32),
            "riscv64" => Ok(Architecture::Riscv64),
            "ppc" => Ok(Architecture::Powerpc),
            "m68k" => Ok(Architecture::M68k),
            "sparc" => Ok(Architecture::Sparc64),
            _ => Err(()),
        }
    }
}

impl Architecture {
    /// Given an Architecture, return the toolchain triplet
    pub fn triplet(&mut self) -> Option<String> {
        match self {
            Architecture::X86gcc2 => Some("i586-pc-haiku".to_string()),
            Architecture::X86 => Some("i586-pc-haiku".to_string()),
            Architecture::Arm64 => Some("aarch64-unknown-haiku".to_string()),
            Architecture::Sparc64 => Some("sparc64-unknown-haiku".to_string()),
            Architecture::Powerpc => Some("powerpc-unknown-haiku".to_string()),
            _ => Some(format!("{}-unknown-haiku", self).to_string()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn test_architecture_into() {
        assert_eq!(Architecture::from_str("x86_64"), Ok(Architecture::X86_64));
        assert_eq!(Architecture::from_str("x86_gcc2"), Ok(Architecture::X86gcc2));
    }

    #[test]
    fn test_architecture_triplets() {
        assert_eq!(Architecture::from_str("x86_64").unwrap().triplet(), Some("x86_64-unknown-haiku".to_string()));
        assert_eq!(Architecture::from_str("x86_gcc2").unwrap().triplet(), Some("i586-pc-haiku".to_string()));
        assert_eq!(Architecture::from_str("x86").unwrap().triplet(), Some("i586-pc-haiku".to_string()));
    }
}
