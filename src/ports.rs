
use std::{env,fs};
use std::io::{BufReader, BufRead};
use std::error::Error;
use std::path::{Path, PathBuf};
use std::fs::File;
use std::time::{SystemTime, UNIX_EPOCH, Duration};

use glob::glob;
use git2::*;

use chrono::offset::Utc;
use chrono::DateTime;

use diesel::{prelude::*, QueryDsl};

use crate::db::PgPool;
use crate::recipe::{NewRecipe,Recipe};

/// A git repository of ports
pub struct PortsRepo {
    db: PgPool,
    pub repo_branch: String,
    pub repo_url: String,
    pub repo_path: PathBuf,
}


impl PortsRepo {
    /// Returns a new PortsRepo object. Configuration is based on .env
    /// variables CACHE_DIR (local cache directory) and PORTS_REPO (remote git repo)
    pub fn new(db: PgPool) -> PortsRepo {
        let cache_dir = env::var("CACHE_DIR").expect("CACHE_DIR must be set");
        let ports_dir = format!("{}/{}", cache_dir, "ports");

        // TODO: Accept other branches

        PortsRepo {
            db: db,
            repo_branch: env::var("PORTS_REPO_BRANCH").unwrap_or("master".to_string()),
            repo_url: env::var("PORTS_REPO").expect("PORTS_REPO must be set"),
            repo_path: PathBuf::from(ports_dir),
        }
    }

    /// Provided a recipe, returns the last change
    ///
    /// # Arguments
    ///
    /// * `path` - Path to the recipe to be scanned
    fn recipe_modified(path: PathBuf) -> Result<(SystemTime,String), Box<dyn Error>> {
        let repo = git2::Repository::discover(path.clone())?;
        let prefix = match repo.workdir() {
            Some(d) => d,
            None => return Err(From::from("Unable to determine git workdir!")),
        };
        let rel_path = path.strip_prefix(prefix)?;
        let mut revwalk = repo.revwalk()?;
        revwalk.push_head()?;
        revwalk.set_sorting(git2::Sort::TOPOLOGICAL | git2::Sort::TIME)?;
        let mut last_change: Option<git2::Commit> = None;
        for rev in revwalk {
            let commit = repo.find_commit(rev?)?;
            let child_obj = commit.as_object();
            let child_tree = child_obj.peel_to_tree()?;
            let parents = commit.parents();
            for parent_commit in parents {
                let parent_obj = parent_commit.as_object();
                let parent_tree = parent_obj.peel_to_tree()?;
                let mut diff_opts = DiffOptions::new();
                diff_opts.pathspec(rel_path);
                let diff = repo.diff_tree_to_tree(Some(&parent_tree), Some(&child_tree), Some(&mut diff_opts))?;
                for delta in diff.deltas() {
                    let newfile = delta.new_file();
                    let file = match newfile.path() {
                        None => continue,
                        Some(f) => f,
                    };
                    if rel_path == file {
                        // Found a modification!
                        last_change = Some(commit.clone());
                    }
                    break;
                }
                if last_change.is_some() {
                    break;
                }
            }
            if last_change.is_some() {
                break;
            }
        }
        if last_change.is_none() {
            return Err(From::from(format!("Unable to locate {:?} in history!", rel_path)))
        }
        let golden_commit = last_change.unwrap();
        let author = match golden_commit.author().name() {
            Some(n) => n.to_string(),
            None => "Unknown".to_string(),
        };
        let epoch = golden_commit.time().seconds() as u64;
        let time = UNIX_EPOCH + Duration::from_secs(epoch);
        Ok((time, author))
    }

    /// Provided a recipe, returns the detected revision via file scanning
    ///
    /// # Arguments
    ///
    /// * `path` - Path to the recipe to be scanned
    fn recipe_revision(path: PathBuf) -> Result<i32, Box<dyn Error>> {
        if path.is_dir() {
            return Err(From::from(format!("{:?} isn't a file!", path.into_os_string())));
        }
        let recipe = File::open(&path)?;
        for (_, line) in BufReader::new(recipe).lines().enumerate() {
            let line = line?;
            if line.starts_with("REVISION") {
                let fields: Vec<&str> = line.split("=").collect();
                let revision = match fields.last() {
                    Some(r) => r.replace("\"", ""),
                    None => {
                        return Err(From::from(format!("{:?} doesn't contain a known revision!",
                            &path.into_os_string())));
                    },
                };
                return Ok(revision.parse::<i32>()?);
            }
        }
        return Err(From::from(format!("{:?} doesn't contain a known revision!", &path.into_os_string())));
    }

    /// Pulls updates from remote git port repository to local cache
    fn update(&self, branch: String) -> Result<bool, Box<dyn Error>> {
        println!("Ports: pulling repo updates...");
        let repo = git2::Repository::discover(&self.repo_path)?;
        repo.find_remote("origin")?.fetch(&[&branch], None, None)?;
        let remote_ref = format!("refs/remotes/origin/{}", &branch);
        let oid = repo.refname_to_id(&remote_ref)?;
        let object = repo.find_object(oid, None).unwrap();
        repo.reset(&object, git2::ResetType::Hard, None)?;
        Ok(true)
    }

    /// Performs an initial clone of a remote git repository to a local cache
    fn repo_clone(&self) -> Result<bool, Box<dyn Error>> {
        println!("Ports: cloning ports repo {}...", &self.repo_url);
        fs::create_dir(self.repo_path.as_path())?;
        Repository::clone(&self.repo_url, &self.repo_path)?;
        println!("Ports: ports repo clone complete.");
        Ok(true)
    }

    /// Checks the local cache for an up-to-date clone of a remote git ports repository
    pub fn check(&self, branch: String) -> Result<bool, Box<dyn Error>> {
        if !Path::new(self.repo_path.as_path()).exists() {
            self.repo_clone()?;
        }
        return self.update(branch);
    }

    /// Scans the local cached ports repository for updates.
    ///
    /// Returns a Vec containing current inventory of recipies in ports repository.
    pub fn scan(&self) -> Result<Vec<NewRecipe>, Box<dyn Error>> {
        println!("Ports: scan request!");

        // Read port groups
        let mut groups: Vec<String> = Vec::new();
        for entry in fs::read_dir(&self.repo_path)? {
            let entry = entry?;
            let path = entry.path();
            if !path.is_dir() {
                continue;
            }
            let group = match path.file_name() {
                Some(s) => s.to_os_string().to_string_lossy().to_string(),
                None => continue,
            };
            // Package groups must not be hidden
            if group.starts_with(".") {
                continue;
            }
            // Package groups must contain '-'
            if !group.contains("-") {
                continue;
            }
            groups.push(group);
        }
        println!("Ports: Found {} groups.", groups.len());

        let mut recipes: Vec<NewRecipe> = Vec::new();
        for group in &groups {
            let focus_group = &self.repo_path.join(group).into_os_string().to_string_lossy().to_string();
            for entry in glob(format!("{}/**/*.recipe", focus_group).as_str())? {
                let entry = entry?;
                let full_path = match entry.parent() {
                    Some(p) => p,
                    None => continue,
                };
                let filename = match entry.file_name() {
                    Some(f) => f.to_os_string().to_string_lossy().to_string(),
                    None => continue,
                };
                let recipe_name = match entry.file_stem() {
                    Some(f) => f.to_os_string().to_string_lossy().to_string(),
                    None => continue,
                };
                let recipe_parts: Vec<&str> = recipe_name.split("-").collect();
                let name = match recipe_parts.first() {
                    Some(n) => n,
                    None => continue,
                };
                let version = match recipe_parts.last() {
                    Some(v) => v,
                    None => continue,
                };
                let (modified,author) = match PortsRepo::recipe_modified(entry.clone()) {
                    Ok(v) => v,
                    Err(e) => {
                        println!("recipe_modified: {}", e);
                        continue;
                    }
                };
                let revision = match PortsRepo::recipe_revision(entry.clone()) {
                    Ok(v) => v,
                    Err(e) => {
                        println!("recipe_revision: {}", e);
                        continue;
                    }
                };
                let rel_path = full_path.strip_prefix(self.repo_path.clone())?;
                let rel_path_string = match rel_path.to_str() {
                    Some(rp) => rp.to_string(),
                    None => continue,
                };
                let datetime: DateTime<Utc> = modified.into();
                println!("Found {} version {}-{} updated by {}, at {}",
                    rel_path_string, version, revision,
                    author, datetime.format("%m/%d/%Y %T"));
                recipes.push(NewRecipe {
                    name: name.to_string(),
                    version: version.to_string(),
                    category: focus_group.clone(),
                    revision: revision,
                    location: format!("{}/{}", rel_path_string, filename),
                    updated_at: modified,
                    updated_by: author,
                    branch: self.repo_branch.clone(),
                });
            }
        }
        println!("Ports: Found {} recipes!", recipes.len());
        Ok(recipes)
    }

    pub fn sync(&self) -> Result<bool, Box<dyn Error>> {
        let sync_start = SystemTime::now();
        use crate::db_schema::recipes;

        // Check for updates to ports repo
        self.check(self.repo_branch.clone())?;

        // Scan for recipes
        let git_recipes = self.scan()?;

        let connection = self.db.get().unwrap();
        for git_recipe in git_recipes.iter() {
            let existing: Vec<Recipe> = recipes::table
                .filter(recipes::name.like(&git_recipe.name))
                .filter(recipes::version.like(&git_recipe.version))
                .load::<Recipe>(&connection)
                .expect("Error examining recipes!");
            if existing.len() > 1 {
                // TODO: I wanna speak to your manager
                println!("More than one existing recipe for {}-{}\n",
                    git_recipe.name, git_recipe.version);
                continue;
            }
            if existing.len() == 0 {
                println!("Inserting new recipe {}-{}", git_recipe.name, git_recipe.version);
                diesel::insert_into(recipes::table)
                    .values(git_recipe)
                    .execute(&connection)?;
                continue;
            }
            let known = existing.first().unwrap();
            if known.revision != git_recipe.revision {
                println!("Updating recipe {}-{} revision {} -> {}",
                    known.name, known.version, known.revision, git_recipe.revision);
                // TODO: Update revision
            }
        }
        println!("Recipe sync execution complete in {} seconds.",
            sync_start.elapsed()?.as_secs());
        return Ok(true);
    }
}
