use crate::graphql_schema::Context;

use diesel::{prelude::*, QueryDsl};

use crate::recipe::Recipe;
use crate::db_schema::jobs;

#[derive(juniper::GraphQLInputObject, Insertable, Debug)]
#[table_name = "jobs"]
pub struct NewJob {
  pub worker_id: i32,
  pub recipe_id: i32,
  pub architecture: String,
  pub state: String,
}

#[derive(Queryable)]
pub struct Job {
  pub id: i32,
  pub worker_id: i32,
  pub recipe_id: i32,
  pub architecture: String,
  pub state: String,
}

#[juniper::object(Context = Context, description = "A build job")]
impl Job {
  pub fn id(&self) -> i32 {
    self.id
  }

  pub fn architecture(&self) -> &str {
    self.architecture.as_str()
  }

  pub fn state(&self) -> &str {
    self.state.as_str()
  }

  pub fn recipe(&self, context: &Context) -> Recipe {
    use crate::db_schema::recipes;

    let connection = context.db.get().unwrap();
    recipes::table
      .find(self.id)
      .get_result::<Recipe>(&connection)
      .expect("Error loading recipe")
  }
}
