extern crate dotenv;

use diesel::prelude::*;

use juniper::RootNode;

use crate::db::PgPool;
use crate::db_schema::workers;

use crate::recipe::Recipe;
use crate::worker::{NewWorker, Worker};

#[derive(Clone)]
pub struct Context {
  pub db: PgPool,
}

impl juniper::Context for Context {}

pub struct QueryRoot;

#[juniper::object(Context = Context)]
impl QueryRoot {
  fn workers(context: &Context) -> Vec<Worker> {
    use crate::db_schema::workers::dsl::*;
    let connection = context.db.get().unwrap();
    workers
      .limit(100)
      .load::<Worker>(&connection)
      .expect("Error loading workers")
  }

  fn recipes(context: &Context) -> Vec<Recipe> {
    use crate::db_schema::recipes::dsl::*;
    let connection = context.db.get().unwrap();
    recipes
      .load::<Recipe>(&connection)
      .expect("Error loading recipes")
  }
}

pub struct MutationRoot;

#[juniper::object(Context = Context)]
impl MutationRoot {
  fn create_worker(context: &Context, data: NewWorker) -> Worker {
    let connection = context.db.get().unwrap();
    diesel::insert_into(workers::table)
      .values(&data)
      .get_result(&connection)
      .expect("Error saving new worker")
  }
}

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
  Schema::new(QueryRoot {}, MutationRoot {})
}
