extern crate chrono;
#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate juniper;
extern crate glob;

use std::io;
use std::sync::Arc;

use actix_files as fs;
use actix_web::{web, App, Error, HttpResponse, HttpServer};
use dotenv::dotenv;
use futures::future::Future;
use juniper::http::graphiql::graphiql_source;
use juniper::http::GraphQLRequest;

mod arch;
mod db;
mod db_schema;
mod graphql_schema;
mod ports;
mod recipe;
mod worker;
mod job;

use crate::db::establish_connection;
use crate::graphql_schema::{create_schema, Context, Schema};

fn graphiql() -> HttpResponse {
    let html = graphiql_source("/graphql");
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

fn graphql(
    st: web::Data<Arc<Schema>>,
    ctx: web::Data<Context>,
    data: web::Json<GraphQLRequest>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || {
        let res = data.execute(&st, &ctx);
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    })
    .map_err(Error::from)
    .and_then(|user| {
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(user))
    })
}

fn main() -> io::Result<()> {
    dotenv().ok();
    println!("Starting up...");
    let pool = establish_connection();
    let schema_context = Context { db: pool.clone() };
    let schema = std::sync::Arc::new(create_schema());

    let ports = ports::PortsRepo::new(pool.clone());
    match ports.sync() {
        Ok(_) => println!("Initial port sync complete."),
        Err(e) => {
            panic!("Error: Unable to perform initial port sync! {}", e);
        }
    }

    println!("Launching MapleGlaze Server on 0.0.0.0:8080");
    HttpServer::new(move || {
        App::new()
            .data(schema.clone())
            .data(schema_context.clone())
            .service(web::resource("/graphql").route(web::post().to_async(graphql)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql)))
            .service(fs::Files::new("/", "frontend").show_files_listing())
    })
    .bind("0.0.0.0:8080")?
    .run()
}
