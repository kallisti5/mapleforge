table! {
    jobs (id) {
        id -> Int4,
        recipe_id -> Int4,
        worker_id -> Int4,
        architecture -> Varchar,
        state -> Varchar,
    }
}

table! {
    recipes (id) {
        id -> Int4,
        name -> Varchar,
        version -> Varchar,
        category -> Varchar,
        revision -> Int4,
        updated_at -> Timestamp,
        updated_by -> Varchar,
        location -> Varchar,
        branch -> Varchar,
    }
}

table! {
    workers (id) {
        id -> Int4,
        name -> Varchar,
        location -> Varchar,
        architecture -> Varchar,
        version -> Nullable<Varchar>,
        last_heartbeat -> Nullable<Timestamp>,
        token -> Nullable<Varchar>,
    }
}

joinable!(jobs -> recipes (recipe_id));
joinable!(jobs -> workers (worker_id));

allow_tables_to_appear_in_same_query!(
    jobs,
    recipes,
    workers,
);
