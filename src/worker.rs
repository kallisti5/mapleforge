use std::time::SystemTime;

use crate::db_schema::workers;
use diesel::{prelude::*, QueryDsl};

use crate::graphql_schema::Context;
use crate::job::Job;

#[derive(Queryable)]
pub struct Worker {
  pub id: i32,
  pub name: String,
  pub location: String,
  pub architecture: String,
  pub version: Option<String>,
  pub last_heartbeat: Option<SystemTime>,
  pub token: Option<String>,
}

#[derive(juniper::GraphQLInputObject, Insertable)]
#[table_name = "workers"]
pub struct NewWorker {
  pub name: String,
  pub location: String,
  pub architecture: String,
}

#[juniper::object(Context = Context, description = "A Haiku worker")]
impl Worker {
  pub fn id(&self) -> i32 {
    self.id
  }

  pub fn name(&self) -> &str {
    self.name.as_str()
  }

  pub fn location(&self) -> &str {
    self.location.as_str()
  }

  pub fn architecture(&self) -> &str {
    self.architecture.as_str()
  }

  pub fn version(&self) -> &str {
    match &self.version {
        Some(v) => v.as_str(),
        None => "unknown",
    }
  }

  pub fn health(&self) -> &str {
    let last_heartbeat = match self.last_heartbeat {
      Some(b) => b,
      None => return "initializing",
    };
    let age = match last_heartbeat.elapsed() {
      Ok(u) => u.as_secs() as i32,
      Err(_) => return "unknown",
    };
    if age >= 300 {
        return "missing";
    }
    "healthy"
  }

  pub fn jobs(&self, context: &Context) -> Vec<Job> {
    use crate::db_schema::jobs;

    let connection = context.db.get().unwrap();
    jobs::table
      .filter(jobs::worker_id.eq(self.id))
      .limit(100)
      .load::<Job>(&connection)
      .expect("Error loading jobs")
  }
}
