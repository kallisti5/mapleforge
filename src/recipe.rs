use std::time::SystemTime;

use crate::graphql_schema::Context;
use crate::db_schema::recipes;
use crate::job::{Job,NewJob};

use diesel::{prelude::*, QueryDsl};

#[derive(Insertable,Debug)]
#[table_name = "recipes"]
pub struct NewRecipe {
  pub name: String,
  pub version: String,
  pub category: String,
  pub revision: i32,
  pub updated_at: SystemTime,
  pub updated_by: String,
  pub location: String,
  pub branch: String,
}

#[derive(Queryable,Debug)]
pub struct Recipe {
  pub id: i32,
  pub name: String,
  pub version: String,
  pub category: String,
  pub revision: i32,
  pub updated_at: SystemTime,
  pub updated_by: String,
  pub location: String,
  pub branch: String,
}

#[juniper::object(Context = Context, description = "A Haiku port recipe")]
impl Recipe {
    pub fn id(&self) -> i32 {
      self.id
    }

    pub fn name(&self) -> &str {
      self.name.as_str()
    }

    pub fn version(&self) -> &str {
      self.version.as_str()
    }

    pub fn revision(&self) -> i32 {
      self.revision
    }

    pub fn location(&self) -> &str {
      self.location.as_str()
    }

    pub fn full_version(&self) -> String {
        return format!("{}-{}", self.version, self.revision).to_string();
    }

    pub fn age(&self) -> i32 {
        match self.updated_at.elapsed() {
            Ok(u) => u.as_secs() as i32,
            Err(_) => 0,
        }
    }

    pub fn trigger(&self, context: &Context) -> Job {
      use crate::db_schema::jobs;
      let job = NewJob {
          worker_id: 1,
          recipe_id: self.id,
          state: "queued".to_string(),
          architecture: "x86_64".to_string(),
      };

      println!("Triggering new build job of recipe {}-{}", self.name, self.version);
      let connection = context.db.get().unwrap();
      diesel::insert_into(jobs::table)
        .values(job)
        .get_result::<Job>(&connection)
        .expect("Failed to insert job into queue")
    }

    pub fn jobs(&self, context: &Context) -> Vec<Job> {
      use crate::db_schema::jobs;

      let connection = context.db.get().unwrap();
      jobs::table
        .filter(jobs::recipe_id.eq(self.id))
        .limit(100)
        .load::<Job>(&connection)
        .expect("Error loading jobs")
    }
}
