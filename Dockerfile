FROM docker.io/rust:alpine3.15 as build

COPY ./ ./

RUN mkdir -p /build-out
RUN apk add --no-cache openssl-dev libpq-dev libgit2-dev sqlite-dev mariadb-connector-c mariadb-dev musl-dev

RUN cargo build --release
RUN cargo install diesel_cli
RUN cp target/release/mapleforge /build-out/
RUN cp /usr/local/cargo/bin/diesel /build-out/

# Final Fedora container
FROM docker.io/alpine:3.15

RUN mkdir -p /mapleforge
RUN apk add --no-cache openssl libpq libgit2 mariadb-connector-c

COPY --from=build /build-out/mapleforge /mapleforge/mapleforge
COPY --from=build /build-out/diesel /usr/local/bin/diesel
ADD migrations /mapleforge/migrations
ADD frontend /mapleforge/frontend
ADD entry /entry

EXPOSE 8080/tcp

CMD /entry
