function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  var keys = Object.keys(data);
  for (var i=0; i<keys.length; i++) {
    let th = document.createElement("th");
    let text = document.createTextNode(keys[i]);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTableRow(table, data) {
  let row = table.insertRow();
  var values = Object.values(data);
  for (var i=0; i<values.length; i++) {
    let td = document.createElement("td");
	if (Array.isArray(values[i])) {
      let text = document.createTextNode(values[i].length);
      td.appendChild(text);
      row.appendChild(td);
	} else {
      let text = document.createTextNode(values[i]);
      if (Number.isInteger(values[i])) {
        // do nothing
      } else if (values[i].toLowerCase() == "unknown") {
        td.style.color = "#999999";
      } else if (values[i].toLowerCase() == "healthy") {
        td.style.color = "green";
        td.style.fontWeight = "bold";
      } else if (values[i].toLowerCase() == "initializing") {
        td.style.color = "blue";
        td.style.fontWeight = "bold";
      } else if (values[i].toLowerCase() == "fault") {
        td.style.color = "red";
        td.style.fontWeight = "bold";
      }
      td.appendChild(text);
      row.appendChild(td);
    }
  }
}

function generateTable(table, datas) {
  if (datas.length == 0) {
    let row = table.insertRow();
    let td = document.createElement("td");
    let text = document.createTextNode("No workers available!");
    td.style.textAlign = "center";
    td.appendChild(text);
    row.appendChild(td);
    return;
  }
  generateTableHead(table, datas[0]);
  let tbody = table.createTBody();
  datas.forEach((data) => {
    generateTableRow(tbody, data);
  });
}

function refreshWorkers(keyword) {
  const options = {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      query: '{ workers { name location architecture version jobs{id} health } }'
    })
  };

  fetch(`/graphql`, options)
    .then(res => res.json())
    .then(renderWorkers);
}

const workerContainer = document.querySelector("#worker-container");

const renderWorkers = ({ data }) => {
  const { workers = [] } = data;
  while (workerContainer.firstChild) {
    workerContainer.removeChild(workerContainer.firstChild);
  }
  const workerFragment = document.createDocumentFragment();
  const workerList = document.createElement('table');
  workerList.className = "u-full-width";
  workerFragment.appendChild(workerList);
  workerContainer.appendChild(workerFragment);
  generateTable(workerList, workers);
}

document.onload = refreshWorkers("");
window.setInterval(function(){refreshWorkers("")}, 5000);
