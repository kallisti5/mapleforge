function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  var keys = Object.keys(data);
  for (var i=0; i<keys.length; i++) {
    let th = document.createElement("th");
    let text = document.createTextNode(keys[i]);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTableRow(table, data) {
  let row = table.insertRow();
  var values = Object.values(data);
  for (var i=0; i<values.length; i++) {
    let td = document.createElement("td");
	if (Array.isArray(values[i])) {
      let text = document.createTextNode(values[i].length);
      td.appendChild(text);
      row.appendChild(td);
	} else {
      let text = document.createTextNode(values[i]);
      if (Number.isInteger(values[i])) {
        // do nothing
      } else if (values[i].toLowerCase() == "unknown") {
        td.style.color = "#999999";
      } else if (values[i].toLowerCase() == "healthy") {
        td.style.color = "green";
        td.style.fontWeight = "bold";
      } else if (values[i].toLowerCase() == "initializing") {
        td.style.color = "blue";
        td.style.fontWeight = "bold";
      } else if (values[i].toLowerCase() == "fault") {
        td.style.color = "red";
        td.style.fontWeight = "bold";
      }
      td.appendChild(text);
      row.appendChild(td);
    }
  }
}

function generateTable(table, datas) {
  if (datas.length == 0) {
    let row = table.insertRow();
    let td = document.createElement("td");
    let text = document.createTextNode("No recipes available!");
    td.style.textAlign = "center";
    td.appendChild(text);
    row.appendChild(td);
    return;
  }
  generateTableHead(table, datas[0]);
  let tbody = table.createTBody();
  datas.forEach((data) => {
    generateTableRow(tbody, data);
  });
}

function refreshWorkers(keyword) {
  const options = {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      query: '{ recipes { name location fullVersion age } }'
    })
  };

  fetch(`/graphql`, options)
    .then(res => res.json())
    .then(renderWorkers);
}

const recipeContainer = document.querySelector("#recipe-container");

const renderWorkers = ({ data }) => {
  const { recipes = [] } = data;
  while (recipeContainer.firstChild) {
    recipeContainer.removeChild(recipeContainer.firstChild);
  }
  const recipeFragment = document.createDocumentFragment();
  const recipeList = document.createElement('table');
  recipeList.className = "u-full-width";
  recipeFragment.appendChild(recipeList);
  recipeContainer.appendChild(recipeFragment);
  generateTable(recipeList, recipes);
}

document.onload = refreshWorkers("");
window.setInterval(function(){refreshWorkers("")}, 5000);
