# Mapleforge

[![Docker Repository on Quay](https://quay.io/repository/kallisti5/mapleforge/status "Docker Repository on Quay")](https://quay.io/repository/kallisti5/mapleforge)

A recipe build system for Haiku.

# Development

## Requirements

* Rust 2021
* PostgreSQL development libraries
* cargo install diesel_cli

## Setup

* Fill out .env with the required variables
  * ```POSTGRESQL_URL="postgres://postgres@127.0.0.1:5432"```
  * ```PORTS_REPO="https://github.com/haikuports/haikuports.git"```
  * ```CACHE_DIR="/tmp/mapleforge"```
* Fill out any optional variables
  * ```PORTS_REPO_BRANCH="master"```
* Setup ```diesel db setup```
* Perform migration ```diesel migration run```

## Build

* cargo build

## Architecture

* Rust-based server which automatically monitors the specified Haiku ports git repository for updates in recipe knowledge
* A Postgres database contains the latest recipe information
* A GraphQL API interface is presented for web clients and workers

## GraphQL examples

http://127.0.0.1:8080/graphiql

```gql
mutation CreateDemoWorker {
  createWorker(data: {name: "test", location: "austin", architecture: "x86_64"}) {
    name
  }
}

query listworkers {
  workers {
    name
    architecture
    location
    jobs {
      id
    }
  }
}
```

# Production releases

Docker images are automatically built on quay.io. The image requires a few environment variables to run.
These are dumped into .env at runtime.

* POSTGRESQL_URL (required)
* PORTS_REPO (optional, default https://github.com/haikuports/haikuports.git)
* CACHE_DIR (optional, default "/tmp/mapleforge")

## Future

These are long-term todo's this project would like to accomplish

* Friendly seperate web ui presenting the GraphQL to users for analysis
* Establishing a standard for haikuporter to run in "headless worker mode" on builders
* Performing recipe lint
* Inventory of compiled packages and their states
